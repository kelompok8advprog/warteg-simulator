Status master
[![pipeline status](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/master/pipeline.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/master)
[![coverage report](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/master/coverage.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/master)

Status branchpavo
[![pipeline status](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchpavo/pipeline.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchpavo)
[![coverage report](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchpavo/coverage.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchpavo)

Status branchsalsa
[![pipeline status](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchsalsa/pipeline.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchsalsa)
[![coverage report](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchsalsa/coverage.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchsalsa)

Status branchabi
[![pipeline status](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchabi/pipeline.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchabi)
[![coverage report](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchabi/coverage.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchabi)

Status branchcarlo
[![pipeline status](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchabi/pipeline.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchcarlo)
[![coverage report](https://gitlab.com/kelompok8advprog/warteg-simulator/badges/branchabi/coverage.svg)](https://gitlab.com/kelompok8advprog/warteg-simulator/-/commits/branchcarlo)

Link: 
wartegsimulator.herokuapp.com