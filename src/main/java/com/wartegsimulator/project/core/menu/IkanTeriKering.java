package com.wartegsimulator.project.core.menu;
import java.lang.Math.*;

public class IkanTeriKering extends MenuTidakPokok {
    Menu menu;
    public IkanTeriKering(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Ikan Teri Kering";

        //makanan dengan porsi yang harus utuh sehingga dibulatkan
        this.porsi=Math.ceil(porsi);
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + Math.ceil(porsi)*3000.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + Math.ceil(porsi)*170.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return Math.ceil(porsi)*3000.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return Math.ceil(porsi)*170.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}

}
