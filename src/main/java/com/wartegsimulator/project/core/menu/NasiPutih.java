package com.wartegsimulator.project.core.menu;

public class NasiPutih extends Menu {

    public NasiPutih(double porsi){
        this.deskripsi="Nasi Putih";
        this.porsi=porsi;
    }

    @Override
    public double getKalori() {
        return porsi*204.0;
    }

    @Override
    public double getHarga() {
        return porsi*4000.0;
    }
}
