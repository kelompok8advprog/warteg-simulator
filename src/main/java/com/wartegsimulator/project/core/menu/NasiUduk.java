package com.wartegsimulator.project.core.menu;

public class NasiUduk extends Menu {

    public NasiUduk(double porsi){
        this.deskripsi="Nasi Uduk";
        this.porsi=porsi;
    }

    @Override
    public double getKalori() {
        return porsi*163.0;
    }

    @Override
    public double getHarga() {
        return porsi*5000.0;
    }
}
