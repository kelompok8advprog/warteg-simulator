package com.wartegsimulator.project.core.menu;

public class SayurNangka extends MenuTidakPokok {

    Menu menu;
    public SayurNangka(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Sayur Nangka";
        this.porsi=porsi;
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + porsi*3000.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + porsi*66.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return porsi*3000.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return porsi*66.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}
}
