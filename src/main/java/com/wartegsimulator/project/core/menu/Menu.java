package com.wartegsimulator.project.core.menu;

public abstract class Menu {

    protected double porsi;
    protected String deskripsi;

    public String getDeskripsi(){ return deskripsi; }

    public double getPorsi(){return porsi;}

    public void tambahPorsi(double porsiTambahan){
        porsi+=porsiTambahan;}

    public abstract double getHarga();

    public abstract double getKalori();

}
