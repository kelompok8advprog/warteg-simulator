package com.wartegsimulator.project.core.menu;

public class SayurLodeh extends MenuTidakPokok {
    Menu menu;
    public SayurLodeh(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Sayur Lodeh";
        this.porsi=porsi;
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + porsi*3000.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + porsi*162.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return porsi*3000.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return porsi*162.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}
}
