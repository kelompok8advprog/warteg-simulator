package com.wartegsimulator.project.core.menu;
import java.lang.Math.*;

public class TehManis extends MenuTidakPokok {
    Menu menu;
    public TehManis(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Teh Manis";

        //makanan dengan porsi yang harus utuh sehingga dibulatkan
        this.porsi=Math.ceil(porsi);
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + Math.ceil(porsi)*2500.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + Math.ceil(porsi)*55.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return Math.ceil(porsi)*2500.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return Math.ceil(porsi)*55.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}

}
