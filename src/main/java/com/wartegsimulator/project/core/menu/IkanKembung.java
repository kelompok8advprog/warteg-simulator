package com.wartegsimulator.project.core.menu;
import java.lang.Math.*;

public class IkanKembung extends MenuTidakPokok {
    Menu menu;
    public IkanKembung(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Ikan Kembung";

        //makanan dengan porsi yang harus utuh sehingga dibulatkan
        this.porsi=Math.ceil(porsi);
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + Math.ceil(porsi)*6000.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + Math.ceil(porsi)*167.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return Math.ceil(porsi)*6000.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return Math.ceil(porsi)*167.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}

}
