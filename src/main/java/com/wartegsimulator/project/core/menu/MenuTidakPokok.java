package com.wartegsimulator.project.core.menu;

public abstract class MenuTidakPokok extends Menu {

    public abstract double getHargaMenuTunggal();

    public abstract double getKaloriMenuTunggal();

    public abstract String getDeskripsiMenuTunggal();

}
