package com.wartegsimulator.project.core.menu;

public class SayurSup extends MenuTidakPokok {

    Menu menu;
    public SayurSup(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Sayur Sup";
        this.porsi=porsi;
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + porsi*3000.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + porsi*56.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return porsi*3000.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return porsi*56.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}
}
