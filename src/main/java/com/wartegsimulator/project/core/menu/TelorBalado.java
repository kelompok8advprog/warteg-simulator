package com.wartegsimulator.project.core.menu;
import java.lang.Math.*;

public class TelorBalado extends MenuTidakPokok {
    Menu menu;
    public TelorBalado(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Telor Balado";

        //makanan dengan porsi yang harus utuh sehingga dibulatkan
        this.porsi=Math.ceil(porsi);
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + Math.ceil(porsi)*5000.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + Math.ceil(porsi)*71.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return Math.ceil(porsi)*5000.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return Math.ceil(porsi)*71.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}

}
