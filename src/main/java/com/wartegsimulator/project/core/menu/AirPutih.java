package com.wartegsimulator.project.core.menu;
import java.lang.Math.*;

public class AirPutih extends MenuTidakPokok {
    Menu menu;
    public AirPutih(Menu menu,double porsi){
        this.menu=menu;
        this.deskripsi="Air Putih";

        //makanan dengan porsi yang harus utuh sehingga dibulatkan
        this.porsi=Math.ceil(porsi);
    }

    @Override
    public double getHarga(){
        return menu.getHarga() + 0.0;
    }

    @Override
    public double getKalori(){
        return menu.getKalori() + 0.0;
    }

    @Override
    public String getDeskripsi(){return menu.getDeskripsi() +", "+ deskripsi;}

    @Override
    public double getHargaMenuTunggal() {
        return 0.0;
    }

    @Override
    public double getKaloriMenuTunggal() {
        return 0.0;
    }

    @Override
    public String getDeskripsiMenuTunggal(){return deskripsi;}

}
