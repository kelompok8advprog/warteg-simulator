package com.wartegsimulator.project.core.menu;
import ch.qos.logback.core.joran.spi.NoAutoStartUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TanpaNasiTest {

    Class<?> menuClass;
    Class<?> tanpaNasiClass;
    TanpaNasi tanpaNasi;

    @BeforeEach
    public void setUp()throws ClassNotFoundException{
        tanpaNasi=new TanpaNasi();
        menuClass=Class.forName("com.wartegsimulator.project.core.menu.Menu");
        tanpaNasiClass=Class.forName("com.wartegsimulator.project.core.menu.TanpaNasi");
    }

    @Test
    public void TestMethodGetHarga()throws NoSuchMethodException{
        Method getHarga=menuClass.getDeclaredMethod("getHarga");
        assertTrue(Modifier.isPublic(getHarga.getModifiers()));
        assertEquals(0,getHarga.getParameterCount());
        assertEquals("double",getHarga.getGenericReturnType().getTypeName());

        double biaya=tanpaNasi.getHarga();
        assertEquals(biaya,0.0);

    }

    @Test
    public void TestMethodGetKalori()throws NoSuchMethodException{
        Method getKalori=menuClass.getDeclaredMethod("getKalori");
        assertTrue(Modifier.isPublic(getKalori.getModifiers()));
        assertEquals(0,getKalori.getParameterCount());
        assertEquals("double",getKalori.getGenericReturnType().getTypeName());

        double kalori=tanpaNasi.getKalori();
        assertEquals(kalori,0.0);

    }

    @Test
    public void TestMethodGetDeskripsi()throws NoSuchMethodException{
        Method getDeskripsi=menuClass.getDeclaredMethod("getDeskripsi");
        assertTrue(Modifier.isPublic(getDeskripsi.getModifiers()));
        assertEquals(0,getDeskripsi.getParameterCount());
        assertEquals("java.lang.String",getDeskripsi.getGenericReturnType().getTypeName());

        String deskripsi=tanpaNasi.getDeskripsi();
        assertEquals(deskripsi,"");

    }



}
