package com.wartegsimulator.project.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SayurLodehTest {

    SayurLodeh sayurLodeh;
    NasiPutih nasiPutih;
    private Class<?> sayurLodehClass;
    private Class<?> menuClass;
    private Class<?> menuTidakPokokClass;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        nasiPutih=new NasiPutih((1.0));
        sayurLodeh= new SayurLodeh(nasiPutih,2.0);
        menuClass=Class.forName("com.wartegsimulator.project.core.menu.Menu");
        menuTidakPokokClass=Class.forName("com.wartegsimulator.project.core.menu.MenuTidakPokok");
        sayurLodehClass = Class.forName("com.wartegsimulator.project.core.menu.SayurLodeh");
    }

    @Test
    public void testGetDeskripsi() throws NoSuchMethodException {
        Method getDeskripsi= menuClass.getDeclaredMethod("getDeskripsi");
        assertTrue(Modifier.isPublic(getDeskripsi.getModifiers()));
        assertEquals("java.lang.String",getDeskripsi.getGenericReturnType().getTypeName());
        assertEquals(getDeskripsi.getParameterCount(),0);
        String deskripsi=sayurLodeh.getDeskripsi();
        assertEquals(deskripsi,"Nasi Putih, Sayur Lodeh");
    }

    @Test
    public void testGetPorsiReturnDoubleValue() throws NoSuchMethodException{
        Method getPorsi=menuClass.getDeclaredMethod("getPorsi");

        assertEquals("double",getPorsi.getGenericReturnType().getTypeName());
        assertEquals(getPorsi.getParameterCount(),0);
        assertTrue(Modifier.isPublic(getPorsi.getModifiers()));

        Double porsi=sayurLodeh.getPorsi();
        assertEquals(porsi,2.0);
    }

    @Test
    public void testGetKaloriReturnDoubleValue() throws NoSuchMethodException{
        Method getKalori=sayurLodehClass.getDeclaredMethod("getKalori");

        assertEquals("double",getKalori.getGenericReturnType().getTypeName());

        assertTrue(Modifier.isPublic(getKalori.getModifiers()));

        assertEquals(0,getKalori.getParameterCount());

        double totalKalori=sayurLodeh.getKalori();

        assertEquals(528.0,totalKalori);

    }


    @Test
    public void testGetHargaReturnDoubleValue() throws NoSuchMethodException{
        Method getHarga=sayurLodehClass.getDeclaredMethod("getHarga");
        assertTrue(Modifier.isPublic(getHarga.getModifiers()));
        assertEquals(0,getHarga.getParameterCount());
        assertEquals("double",getHarga.getGenericReturnType().getTypeName());
        double totalHarga=sayurLodeh.getHarga();
        assertEquals(10000.0,totalHarga);
    }

    @Test
    public void testTambahPorsiUpdatePorsiValue()throws NoSuchMethodException{
        Method tambahPorsi=menuClass.getDeclaredMethod("tambahPorsi",double.class);
        assertTrue(Modifier.isPublic(tambahPorsi.getModifiers()));
        assertEquals(1,tambahPorsi.getParameterCount());
        assertEquals("void",tambahPorsi.getGenericReturnType().getTypeName());
        sayurLodeh.tambahPorsi(1.0);
        assertEquals(sayurLodeh.getPorsi(),3.0);
    }

    @Test
    public void testGetDeskripsiTunggal()throws NoSuchMethodException{
        Method getDeskripsiMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getDeskripsiMenuTunggal");
        assertTrue(Modifier.isPublic(getDeskripsiMenuTunggal.getModifiers()));
        assertEquals(0,getDeskripsiMenuTunggal.getParameterCount());
        assertEquals("java.lang.String",getDeskripsiMenuTunggal.getGenericReturnType().getTypeName());

        String deskripsiTunggal=sayurLodeh.getDeskripsiMenuTunggal();
        assertEquals(deskripsiTunggal,sayurLodeh.deskripsi);
    }

    @Test
    public void testGetKaloriMenuTunggal()throws NoSuchMethodException{
        Method getKaloriMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getKaloriMenuTunggal");
        assertTrue(Modifier.isPublic(getKaloriMenuTunggal.getModifiers()));
        assertEquals(0,getKaloriMenuTunggal.getParameterCount());
        assertEquals("double",getKaloriMenuTunggal.getGenericReturnType().getTypeName());

        double kaloriTunggal=sayurLodeh.getKaloriMenuTunggal();
        assertEquals(kaloriTunggal,324.0);
    }

    @Test
    public void testGetHargaMenuTunggal()throws NoSuchMethodException{
        Method getHargaMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getHargaMenuTunggal");
        assertTrue(Modifier.isPublic(getHargaMenuTunggal.getModifiers()));
        assertEquals(0,getHargaMenuTunggal.getParameterCount());
        assertEquals("double",getHargaMenuTunggal.getGenericReturnType().getTypeName());

        double hargaTunggal=sayurLodeh.getHargaMenuTunggal();
        assertEquals(hargaTunggal,6000.0);
    }
}
