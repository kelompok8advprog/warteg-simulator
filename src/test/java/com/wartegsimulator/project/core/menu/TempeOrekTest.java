package com.wartegsimulator.project.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TempeOrekTest {

    TempeOrek tempeOrek;
    NasiPutih nasiPutih;
    private Class<?> tempeOrekClass;
    private Class<?> menuClass;
    private Class<?> menuTidakPokokClass;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        nasiPutih=new NasiPutih((1.0));
        tempeOrek= new TempeOrek(nasiPutih,2.0);
        menuClass=Class.forName("com.wartegsimulator.project.core.menu.Menu");
        menuTidakPokokClass=Class.forName("com.wartegsimulator.project.core.menu.MenuTidakPokok");
        tempeOrekClass= Class.forName("com.wartegsimulator.project.core.menu.TempeOrek");
    }

    @Test
    public void testGetDeskripsi() throws NoSuchMethodException {
        Method getDeskripsi= menuClass.getDeclaredMethod("getDeskripsi");
        assertTrue(Modifier.isPublic(getDeskripsi.getModifiers()));
        assertEquals("java.lang.String",getDeskripsi.getGenericReturnType().getTypeName());
        assertEquals(getDeskripsi.getParameterCount(),0);
        String deskripsi=tempeOrek.getDeskripsi();
        assertEquals(deskripsi,"Nasi Putih, Tempe Orek");
    }

    @Test
    public void testGetPorsiReturnDoubleValue() throws NoSuchMethodException{
        Method getPorsi=menuClass.getDeclaredMethod("getPorsi");

        assertEquals("double",getPorsi.getGenericReturnType().getTypeName());
        assertEquals(getPorsi.getParameterCount(),0);
        assertTrue(Modifier.isPublic(getPorsi.getModifiers()));

        Double porsi=tempeOrek.getPorsi();
        assertEquals(porsi,2.0);
    }

    @Test
    public void testGetKaloriReturnDoubleValue() throws NoSuchMethodException{
        Method getKalori=tempeOrekClass.getDeclaredMethod("getKalori");

        assertEquals("double",getKalori.getGenericReturnType().getTypeName());

        assertTrue(Modifier.isPublic(getKalori.getModifiers()));

        assertEquals(0,getKalori.getParameterCount());

        double totalKalori=tempeOrek.getKalori();

        assertEquals(554.0,totalKalori);

    }


    @Test
    public void testGetHargaReturnDoubleValue() throws NoSuchMethodException{
        Method getHarga=tempeOrekClass.getDeclaredMethod("getHarga");
        assertTrue(Modifier.isPublic(getHarga.getModifiers()));
        assertEquals(0,getHarga.getParameterCount());
        assertEquals("double",getHarga.getGenericReturnType().getTypeName());
        double totalHarga=tempeOrek.getHarga();
        assertEquals(10000.0,totalHarga);
    }

    @Test
    public void testTambahPorsiUpdatePorsiValue()throws NoSuchMethodException{
        Method tambahPorsi=menuClass.getDeclaredMethod("tambahPorsi",double.class);
        assertTrue(Modifier.isPublic(tambahPorsi.getModifiers()));
        assertEquals(1,tambahPorsi.getParameterCount());
        assertEquals("void",tambahPorsi.getGenericReturnType().getTypeName());
        tempeOrek.tambahPorsi(1.0);
        assertEquals(tempeOrek.getPorsi(),3.0);
    }

    @Test
    public void testGetDeskripsiTunggal()throws NoSuchMethodException{
        Method getDeskripsiMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getDeskripsiMenuTunggal");
        assertTrue(Modifier.isPublic(getDeskripsiMenuTunggal.getModifiers()));
        assertEquals(0,getDeskripsiMenuTunggal.getParameterCount());
        assertEquals("java.lang.String",getDeskripsiMenuTunggal.getGenericReturnType().getTypeName());

        String deskripsiTunggal=tempeOrek.getDeskripsiMenuTunggal();
        assertEquals(deskripsiTunggal,tempeOrek.deskripsi);
    }

    @Test
    public void testGetKaloriMenuTunggal()throws NoSuchMethodException{
        Method getKaloriMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getKaloriMenuTunggal");
        assertTrue(Modifier.isPublic(getKaloriMenuTunggal.getModifiers()));
        assertEquals(0,getKaloriMenuTunggal.getParameterCount());
        assertEquals("double",getKaloriMenuTunggal.getGenericReturnType().getTypeName());

        double kaloriTunggal=tempeOrek.getKaloriMenuTunggal();
        assertEquals(kaloriTunggal,350.0);
    }

    @Test
    public void testGetHargaMenuTunggal()throws NoSuchMethodException{
        Method getHargaMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getHargaMenuTunggal");
        assertTrue(Modifier.isPublic(getHargaMenuTunggal.getModifiers()));
        assertEquals(0,getHargaMenuTunggal.getParameterCount());
        assertEquals("double",getHargaMenuTunggal.getGenericReturnType().getTypeName());

        double hargaTunggal=tempeOrek.getHargaMenuTunggal();
        assertEquals(hargaTunggal,6000.0);
    }
}
