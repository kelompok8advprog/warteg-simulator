package com.wartegsimulator.project.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.mockito.Mockito;


public class MenuTest {
    
    private Class<?> menuClass;
    Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("com.wartegsimulator.project.core.menu.Menu");
        menu=Mockito.mock(Menu.class);
    }

    @Test
    public void testMethodGetDeskripsi() throws NoSuchMethodException {
        Method getDeskripsi=menuClass.getDeclaredMethod("getDeskripsi");
        assertTrue(Modifier.isPublic(getDeskripsi.getModifiers()));
        assertEquals(0,getDeskripsi.getParameterCount());
        assertEquals("java.lang.String",getDeskripsi.getGenericReturnType().getTypeName());

        Mockito.doCallRealMethod().when(menu).getDeskripsi();
        String deskripsi = menu.getDeskripsi();
        assertEquals(null,deskripsi);
    }


    @Test
    public void testMethodGetHarga() throws NoSuchMethodException {
        Method getHarga=menuClass.getDeclaredMethod("getHarga");
        assertTrue(Modifier.isAbstract(getHarga.getModifiers()));
        assertEquals(0,getHarga.getParameterCount());
    }

    @Test
    public void testMethodGetKalori() throws NoSuchMethodException {
        Method getKalori=menuClass.getDeclaredMethod("getKalori");
        assertTrue(Modifier.isAbstract(getKalori.getModifiers()));
        assertEquals(0,getKalori.getParameterCount());
    }

    @Test
    public void testMethodGetPorsi() throws NoSuchMethodException {
        Method getPorsi=menuClass.getDeclaredMethod("getPorsi");
        assertTrue(Modifier.isPublic(getPorsi.getModifiers()));
        assertEquals(0,getPorsi.getParameterCount());
        assertEquals("double",getPorsi.getGenericReturnType().getTypeName());

        Mockito.doCallRealMethod().when(menu).getPorsi();
        double porsi= menu.getPorsi();
        assertEquals(0.0,porsi);
    }

    @Test void testTambahPorsiChangeValueOfPorsi() throws NoSuchMethodException {
        Method tambahPorsi=menuClass.getDeclaredMethod("tambahPorsi",double.class);

        assertTrue(Modifier.isPublic(tambahPorsi.getModifiers()));
        assertEquals(1,tambahPorsi.getParameterCount());
        assertEquals("void",tambahPorsi.getGenericReturnType().getTypeName());

        Mockito.doCallRealMethod().when(menu).tambahPorsi(2.0);
        menu.tambahPorsi(2.0);

        Mockito.doCallRealMethod().when(menu).getPorsi();
        double porsi=menu.getPorsi();
        assertEquals(2.0,porsi);

    }


}
