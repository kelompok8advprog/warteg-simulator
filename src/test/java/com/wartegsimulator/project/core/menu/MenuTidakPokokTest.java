package com.wartegsimulator.project.core.menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.mockito.Mockito;

public class MenuTidakPokokTest {
    private Class<?> menuTidakPokokClass;
    MenuTidakPokok menuTidakPokok;

    @BeforeEach
    public void setUp() throws Exception {
        menuTidakPokokClass= Class.forName("com.wartegsimulator.project.core.menu.MenuTidakPokok");
        menuTidakPokok=Mockito.mock(MenuTidakPokok.class);
    }

    @Test
    public void testMethodGetDeskripsiMenuTunggal() throws NoSuchMethodException {
        Method getDeskripsiMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getDeskripsiMenuTunggal");
        assertTrue(Modifier.isPublic(getDeskripsiMenuTunggal.getModifiers()));
        assertEquals(0,getDeskripsiMenuTunggal.getParameterCount());
        assertEquals("java.lang.String",getDeskripsiMenuTunggal.getGenericReturnType().getTypeName());
    }


    @Test
    public void testMethodGetHargaMenuTunggal() throws NoSuchMethodException {
        Method getHarga=menuTidakPokokClass.getDeclaredMethod("getHargaMenuTunggal");
        assertTrue(Modifier.isAbstract(getHarga.getModifiers()));
        assertEquals(0,getHarga.getParameterCount());
    }

    @Test
    public void testMethodGetKaloriMenuTunggal() throws NoSuchMethodException {
        Method getKaloriMenuTunggal=menuTidakPokokClass.getDeclaredMethod("getKaloriMenuTunggal");
        assertTrue(Modifier.isAbstract(getKaloriMenuTunggal.getModifiers()));
        assertEquals(0,getKaloriMenuTunggal.getParameterCount());
    }




}
