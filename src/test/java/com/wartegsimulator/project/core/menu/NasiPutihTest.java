package com.wartegsimulator.project.core.menu;
import ch.qos.logback.core.joran.spi.NoAutoStartUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NasiPutihTest {

    Class<?> menuClass;
    Class<?> nasiPutihClass;
    NasiPutih nasiPutih;

    @BeforeEach
    public void setUp()throws ClassNotFoundException{
        nasiPutih=new NasiPutih(1.0);
        menuClass=Class.forName("com.wartegsimulator.project.core.menu.Menu");
        nasiPutihClass=Class.forName("com.wartegsimulator.project.core.menu.NasiPutih");
    }

    @Test
    public void TestMethodGetHarga()throws NoSuchMethodException{
        Method getHarga=menuClass.getDeclaredMethod("getHarga");
        assertTrue(Modifier.isPublic(getHarga.getModifiers()));
        assertEquals(0,getHarga.getParameterCount());
        assertEquals("double",getHarga.getGenericReturnType().getTypeName());

        double biaya=nasiPutih.getHarga();
        assertEquals(biaya,4000.0);

    }

    @Test
    public void TestMethodGetKalori()throws NoSuchMethodException{
        Method getKalori=menuClass.getDeclaredMethod("getKalori");
        assertTrue(Modifier.isPublic(getKalori.getModifiers()));
        assertEquals(0,getKalori.getParameterCount());
        assertEquals("double",getKalori.getGenericReturnType().getTypeName());

        double kalori=nasiPutih.getKalori();
        assertEquals(kalori,204.0);

    }

    @Test
    public void TestMethodGetDeskripsi()throws NoSuchMethodException{
        Method getDeskripsi=menuClass.getDeclaredMethod("getDeskripsi");
        assertTrue(Modifier.isPublic(getDeskripsi.getModifiers()));
        assertEquals(0,getDeskripsi.getParameterCount());
        assertEquals("java.lang.String",getDeskripsi.getGenericReturnType().getTypeName());

        String deskripsi=nasiPutih.getDeskripsi();
        assertEquals(deskripsi,"Nasi Putih");

    }



}
